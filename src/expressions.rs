#[cfg(test)]
use crate::idents::arb_symbol;
use crate::idents::{symbol, Symbol};
use crate::numbers::number;
use crate::whitespace::opt_space;
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
#[cfg(test)]
use proptest_derive::Arbitrary;
use std::io::Write;
use winnow::combinator::{alt, delimited, fold_repeat, separated};
use winnow::prelude::*;

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum UnaryOperator {
    LogicNot,
    Minus,
    BitwiseNot,
}

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum BinaryOperator {
    LogicOr,
    LogicAnd,
    BitwiseOr,
    BitwiseAnd,
    Equals,
    NotEquals,
    Lesser,
    Greater,
    LesserOrEquals,
    GreaterOrEquals,
    ShiftRight,
    ShiftLeft,
    Plus,
    Minus,
    Multiply,
    Divide,
    Remainder,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Ident(Symbol<'static>),
    Number(u64),
    Call {
        function: Symbol<'static>,
        arguments: Vec<Expression>,
    },
    UnaryOp {
        operator: UnaryOperator,
        right: Box<Expression>,
    },
    BinaryOp {
        left: Box<Expression>,
        operator: BinaryOperator,
        right: Box<Expression>,
    },
    TernaryOp {
        condition: Box<Expression>,
        left: Box<Expression>,
        right: Box<Expression>,
    },
}

#[cfg(test)]
pub fn arb_expression() -> impl Strategy<Value = Expression> {
    let leaf = prop_oneof![
        arb_symbol().prop_map(Expression::Ident),
        any::<u64>().prop_map(Expression::Number),
    ];
    leaf.prop_recursive(8, 256, 10, |inner| {
        prop_oneof![
            (
                arb_symbol(),
                proptest::collection::vec(inner.clone(), 0..10)
            )
                .prop_map(|(function, arguments)| Expression::Call {
                    function,
                    arguments
                }),
            (any::<UnaryOperator>(), inner.clone()).prop_map(|(operator, right)| {
                Expression::UnaryOp {
                    operator,
                    right: Box::new(right),
                }
            }),
            (inner.clone(), any::<BinaryOperator>(), inner.clone()).prop_map(
                |(left, operator, right)| {
                    Expression::BinaryOp {
                        left: Box::new(left),
                        operator,
                        right: Box::new(right),
                    }
                }
            ),
            (inner.clone(), inner.clone(), inner.clone()).prop_map(|(condition, left, right)| {
                Expression::TernaryOp {
                    condition: Box::new(condition),
                    left: Box::new(left),
                    right: Box::new(right),
                }
            })
        ]
    })
}

impl<W> LdWrite<W> for Expression
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Ident(ident) => {
                ident.ld_write(ctx)?;
            }
            Self::Number(n) => {
                write!(ctx, "{}", n)?;
            }
            Self::Call {
                function,
                arguments,
            } => {
                let mut first = true;
                function.ld_write(ctx)?;

                write!(ctx, "(")?;
                for a in arguments {
                    if !first {
                        write!(ctx, ", ")?;
                    }
                    a.ld_write(ctx)?;
                    first = false;
                }
                write!(ctx, ")")?;
            }
            Self::UnaryOp { operator, right } => {
                use UnaryOperator as U;
                let op: &[u8] = match operator {
                    U::LogicNot => b"!",
                    U::Minus => b"-",
                    U::BitwiseNot => b"~",
                };

                ctx.write_all(op)?;
                write!(ctx, "(")?;
                right.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::BinaryOp {
                left,
                operator,
                right,
            } => {
                use BinaryOperator as B;
                let op: &[u8] = match operator {
                    B::LogicOr => b"||",
                    B::LogicAnd => b"&&",
                    B::BitwiseOr => b"|",
                    B::BitwiseAnd => b"&",
                    B::Equals => b"==",
                    B::NotEquals => b"!=",
                    B::Lesser => b"<",
                    B::Greater => b">",
                    B::LesserOrEquals => b"<=",
                    B::GreaterOrEquals => b">=",
                    B::ShiftRight => b">>",
                    B::ShiftLeft => b"<<",
                    B::Plus => b"+",
                    B::Minus => b"-",
                    B::Multiply => b"*",
                    B::Divide => b"/",
                    B::Remainder => b"%",
                };
                write!(ctx, "(")?;
                left.ld_write(ctx)?;
                write!(ctx, " ")?;
                ctx.write_all(op)?;
                write!(ctx, " ")?;
                right.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::TernaryOp {
                condition,
                left,
                right,
            } => {
                write!(ctx, "(")?;
                condition.ld_write(ctx)?;
                write!(ctx, " ? ")?;
                left.ld_write(ctx)?;
                write!(ctx, " : ")?;
                right.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
        }
        Ok(())
    }
}

fn value_ident(input: &mut &[u8]) -> PResult<Expression> {
    symbol
        .map(|x| {
            // This case is ambiguous an identifier like
            // AAAAh
            // could be considered either a identifier or a number.
            // We will prefer to keep them as idents and not parse them as numbers
            Expression::Ident(x.into_owned())
        })
        .parse_next(input)
}

fn value_number(input: &mut &[u8]) -> PResult<Expression> {
    number.map(|x| Expression::Number(x)).parse_next(input)
}

fn value_nested(input: &mut &[u8]) -> PResult<Expression> {
    delimited("(", wsc!(expression), ")").parse_next(input)
}

fn value_call(input: &mut &[u8]) -> PResult<Expression> {
    let func = symbol.map(|s| s.into_owned()).parse_next(input)?;
    let _ = wsc!("(").parse_next(input)?;
    let args = separated(0.., expression, wsc!(',')).parse_next(input)?;
    let _ = (opt_space, ")").parse_next(input)?;
    Ok(Expression::Call {
        function: func.into(),
        arguments: args,
    })
}

pub fn value(input: &mut &[u8]) -> PResult<Expression> {
    alt((value_nested, value_call, value_ident, value_number)).parse_next(input)
}

fn expr_unary_op(input: &mut &[u8]) -> PResult<Expression> {
    let op = alt(("-", "!", "~")).parse_next(input)?;
    let _ = opt_space.parse_next(input)?;
    let right = expr_level_1.parse_next(input)?;
    Ok(Expression::UnaryOp {
        operator: match op {
            b"-" => UnaryOperator::Minus,
            b"!" => UnaryOperator::LogicNot,
            b"~" => UnaryOperator::BitwiseNot,
            _ => panic!("Invalid operator"),
        },
        right: Box::new(right),
    })
}

fn expr_level_1(input: &mut &[u8]) -> PResult<Expression> {
    alt((expr_unary_op, value)).parse_next(input)
}

fn expr_level_2(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_1.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!(alt(("*", "/", "%"))), expr_level_1),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: match new.0 {
                b"*" => BinaryOperator::Multiply,
                b"/" => BinaryOperator::Divide,
                b"%" => BinaryOperator::Remainder,
                _ => panic!("Invalid operator"),
            },
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_3(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_2.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!(alt(("+", "-"))), expr_level_2),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: match new.0 {
                b"+" => BinaryOperator::Plus,
                b"-" => BinaryOperator::Minus,
                _ => panic!("Invalid operator"),
            },
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_4(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_3.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!(alt(("<<", ">>"))), expr_level_3),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: match new.0 {
                b"<<" => BinaryOperator::ShiftLeft,
                b">>" => BinaryOperator::ShiftRight,
                _ => panic!("Invalid operator"),
            },
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_5(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_4.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!(alt(("==", "!=", "<=", ">=", "<", ">"))), expr_level_4),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: match new.0 {
                b"==" => BinaryOperator::Equals,
                b"!=" => BinaryOperator::NotEquals,
                b"<=" => BinaryOperator::LesserOrEquals,
                b">=" => BinaryOperator::GreaterOrEquals,
                b"<" => BinaryOperator::Lesser,
                b">" => BinaryOperator::Greater,
                _ => panic!("Invalid operator"),
            },
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_6(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_5.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!("&"), expr_level_5),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: BinaryOperator::BitwiseAnd,
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_7(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_6.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!("|"), expr_level_6),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: BinaryOperator::BitwiseOr,
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_8(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_7.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!("&&"), expr_level_7),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: BinaryOperator::LogicAnd,
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_level_9(input: &mut &[u8]) -> PResult<Expression> {
    let first = expr_level_8.parse_next(input)?;
    let fold = fold_repeat(
        0..,
        (wsc!("||"), expr_level_8),
        || first.clone(),
        |prev, new: (&[u8], Expression)| Expression::BinaryOp {
            left: Box::new(prev),
            operator: BinaryOperator::LogicOr,
            right: Box::new(new.1),
        },
    )
    .parse_next(input)?;
    Ok(fold)
}

fn expr_ternary_op(input: &mut &[u8]) -> PResult<Expression> {
    let cond = expr_level_9.parse_next(input)?;
    let _ = wsc!("?").parse_next(input)?;
    let left = expression.parse_next(input)?;
    let _ = wsc!(":").parse_next(input)?;
    let right = expression.parse_next(input)?;
    Ok(Expression::TernaryOp {
        condition: Box::new(cond),
        left: Box::new(left),
        right: Box::new(right),
    })
}

pub fn expression(input: &mut &[u8]) -> PResult<Expression> {
    alt((expr_ternary_op, expr_level_9)).parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    proptest! {
        #[test]
        fn proptest_expression(value in arb_expression()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r =  expression.parse(&v);
            assert_eq!(r, Ok(value));
        }
    }

    #[test]
    fn test_ws() {
        let x = b"a ( b ( d , ( 0 ) ) , c )";
        assert!(expression.parse(x).is_ok());
        let y = b"a(b(d,(0)),c)";
        assert_eq!(expression.parse(x), expression.parse(y));
    }

    #[test]
    fn test_expression() {
        assert!(expression.parse(b"a ( .b ) ? c ( d ) : e").is_ok());

        assert_eq!(
            expression.parse(b"A-B"),
            Ok(Expression::Ident("A-B".try_into().unwrap()))
        );

        assert_eq!(
            expression.parse(b"A - B"),
            Ok(Expression::BinaryOp {
                left: Box::new(Expression::Ident("A".try_into().unwrap())),
                operator: BinaryOperator::Minus,
                right: Box::new(Expression::Ident("B".try_into().unwrap())),
            })
        );
    }
}
