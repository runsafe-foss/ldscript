use winnow::prelude::*;
use winnow::{
    ascii::multispace1,
    combinator::{alt, delimited, repeat},
    token::take_until0,
};

pub fn comment<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    delimited(b"/*", take_until0("*/"), b"*/").parse_next(input)
}

pub fn space_or_comment<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    alt((multispace1, comment)).parse_next(input)
}

pub fn space<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    repeat::<_, _, (), _, _>(1.., space_or_comment)
        .recognize()
        .parse_next(input)
}

pub fn opt_space<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    repeat::<_, _, (), _, _>(0.., space_or_comment)
        .recognize()
        .parse_next(input)
}

/// Transforms a parser to automatically consume whitespace and comments
/// between each token.
macro_rules! wsc(
    ($arg:expr) => ({
        use $crate::whitespace::opt_space;
        use winnow::combinator::delimited;
        delimited(opt_space, $arg, opt_space)
    });

    ($arg0:expr, $($args:expr),+) => ({
        use $crate::whitespace::opt_space;
        use winnow::combinator::delimited;
        delimited(opt_space, ($arg0, $($args),*), opt_space)
    })
);

#[cfg(test)]
mod tests {
    use super::*;
    use winnow::token::take_while;

    fn is_good(c: u8) -> bool {
        c.is_ascii_alphanumeric() || c == b'/' || c == b'*'
    }

    #[test]
    fn test_wsc() {
        let input = b"a /* b */ c / * d /**/ e ";
        assert_eq!(
            repeat(0.., wsc!(take_while(1.., is_good))).parse(input),
            Ok(vec![
                b"a".as_ref(),
                b"c".as_ref(),
                b"/".as_ref(),
                b"*".as_ref(),
                b"d".as_ref(),
                b"e".as_ref()
            ])
        );
    }

    #[test]
    fn test_opt_space() {
        let mut test_parser = ("(", opt_space, take_while(1.., is_good), opt_space, ")");

        let input1 = b"(  a  )";
        assert_done!(test_parser.parse(input1));

        let input2 = b"(a)";
        assert_done!(test_parser.parse(input2));
    }
}
