//! Linker Script parser
//!
//! # Usage
//!
//! ```
//! extern crate ldscript as lds;
//!
//! use std::fs::File;
//! use std::io::Read;
//!
//! fn main() {
//!     let script = &mut Vec::new();
//!     File::open("tests/msp430bt5190.ld").unwrap()
//!                 .read(script).unwrap();
//!
//!     println!("{:#?}", lds::parse(script).unwrap());
//! }
//! ```
//!
//! # References
//!
//! - [GNU binutils documentation](https://sourceware.org/binutils/docs/ld/Scripts.html#Scripts)
//!
#[macro_use]
mod utils;
#[macro_use]
mod whitespace;
mod commands;
mod expressions;
mod idents;
mod memory;
mod numbers;
mod script;
mod sections;
mod statements;

pub use commands::AsNeeded;
pub use commands::Command;
pub use commands::File;
pub use commands::{InputCall, SingleFileCall};
pub use expressions::BinaryOperator;
pub use expressions::Expression;
pub use expressions::UnaryOperator;
pub use idents::{LdFile, LdString, Pattern, Symbol};
pub use memory::Region;
pub use script::RootItem;
pub use sections::DataType;
pub use sections::OutputSectionCommand;
pub use sections::OutputSectionConstraint;
pub use sections::OutputSectionType;
pub use sections::SectionCommand;
pub use sections::SectionPattern;
pub use sections::SectionSymbol;
pub use statements::AssignOperator;
pub use statements::Statement;
use std::io::Write;
use winnow::prelude::*;

pub struct LdCtx<W> {
    indent: usize,
    write: W,
    /// There are some places where prefixed file paths must be quoted.
    /// `SEARCH_DIR` being a notable example.
    quote_file: bool,
}

impl<W> LdCtx<W> {
    pub fn new(write: W) -> LdCtx<W> {
        Self {
            indent: 0,
            write,
            quote_file: true,
        }
    }
}

impl<W> LdCtx<W> {
    fn with_indent<F>(&mut self, mut f: F) -> std::io::Result<()>
    where
        F: FnMut(&mut Self) -> std::io::Result<()>,
    {
        self.indent();
        let r = f(self);
        self.dedent();
        r
    }
    fn indent(&mut self) -> usize {
        let old = self.indent;
        self.indent += 1;
        old
    }
    fn dedent(&mut self) -> usize {
        let old = self.indent;
        self.indent = self.indent.saturating_sub(1);
        old
    }
}

impl<W> LdCtx<W>
where
    W: Write,
{
    pub fn write_literal(&mut self, buf: &[u8]) -> std::io::Result<()> {
        LdCtx::new(&mut self.write).write_all(buf)
    }
}

impl<W> Write for LdCtx<W>
where
    W: Write,
{
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        macro_rules! handle_err {
            ($write:expr, $written:expr) => {
                match $write {
                    Ok(size) => size,
                    Err(_) if $written > 0 => {
                        return Ok($written);
                    }
                    Err(e) => {
                        return Err(e);
                    }
                }
            };
        }

        let mut written = 0;
        let lines = buf.split_inclusive(|c| *c == b'\n');

        for l in lines {
            written += handle_err!(self.write.write(l), written);

            if l.ends_with(&[b'\n']) {
                for _ in 0..self.indent {
                    handle_err!(write!(self.write, "  "), written);
                }
            }
        }

        Ok(written)
    }
    fn flush(&mut self) -> std::io::Result<()> {
        self.write.flush()
    }
}

pub trait LdWrite<W> {
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()>;
}

impl<W, T> LdWrite<W> for Option<T>
where
    W: Write,
    T: LdWrite<W>,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        if let Some(v) = self {
            v.ld_write(ctx)?;
        }
        Ok(())
    }
}

impl<W, T> LdWrite<W> for Box<T>
where
    W: Write,
    T: LdWrite<W>,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        self.as_ref().ld_write(ctx)?;
        Ok(())
    }
}
/// Parses the string that contains a linker script
pub fn parse(ldscript: &[u8]) -> Result<Vec<RootItem>, String> {
    let mut ldscript = ldscript;
    match script::parse.parse(&mut ldscript) {
        Ok(result) => Ok(result),
        //TODO: add error handling
        Err(e) => Err(format!("Parsing failed, error: {:?}", e)),
    }
}
