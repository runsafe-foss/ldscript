#[cfg(test)]
use crate::commands::arb_command;
use crate::commands::{command, command_no_call, Command};
#[cfg(test)]
use crate::memory::arb_region;
use crate::memory::{region, Region};
#[cfg(test)]
use crate::sections::arb_section_command;
use crate::sections::{section_command, SectionCommand};
#[cfg(test)]
use crate::statements::arb_statement;
use crate::statements::{statement, Statement};
use crate::whitespace::opt_space;
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
use std::io::Write;
use winnow::combinator::{alt, repeat};
use winnow::prelude::*;

#[derive(Debug, PartialEq)]
pub enum RootItem {
    Statement(Statement),
    Command(Command),
    Memory { regions: Vec<Region> },
    Sections { list: Vec<SectionCommand> },
}

#[cfg(test)]
fn arb_root_item() -> impl Strategy<Value = RootItem> {
    prop_oneof![
        arb_statement().prop_map(RootItem::Statement),
        arb_command().prop_map(RootItem::Command),
        proptest::collection::vec(arb_region(), 1..10)
            .prop_map(|regions| RootItem::Memory { regions }),
        proptest::collection::vec(arb_section_command(), 1..10)
            .prop_map(|list| RootItem::Sections { list }),
    ]
}

impl<W> LdWrite<W> for RootItem
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Statement(s) => {
                s.ld_write(ctx)?;
                writeln!(ctx)?;
            }
            Self::Command(c) => {
                c.ld_write(ctx)?;
                writeln!(ctx)?;
            }
            Self::Memory { regions } => {
                ctx.with_indent(|ctx| {
                    writeln!(ctx, "MEMORY {{")?;
                    for r in regions {
                        r.ld_write(ctx)?;
                        writeln!(ctx)?;
                    }
                    Ok(())
                })?;
                writeln!(ctx, "}}")?;
            }
            Self::Sections { list } => {
                ctx.with_indent(|ctx| {
                    writeln!(ctx, "SECTIONS {{")?;
                    for r in list {
                        r.ld_write(ctx)?;
                        writeln!(ctx)?;
                    }
                    Ok(())
                })?;
                writeln!(ctx, "}}")?;
            }
        }

        Ok(())
    }
}
fn statement_item(input: &mut &[u8]) -> PResult<RootItem> {
    statement
        .map(|stmt| RootItem::Statement(stmt))
        .parse_next(input)
}

fn early_command_item(input: &mut &[u8]) -> PResult<RootItem> {
    command_no_call
        .map(|cmd| RootItem::Command(cmd))
        .parse_next(input)
}
fn late_command_item(input: &mut &[u8]) -> PResult<RootItem> {
    command.map(|cmd| RootItem::Command(cmd)).parse_next(input)
}

fn memory_item(input: &mut &[u8]) -> PResult<RootItem> {
    let _ = ("MEMORY", wsc!("{")).parse_next(input)?;
    let regions = repeat(1.., wsc!(region)).parse_next(input)?;
    let _ = wsc!("}").parse_next(input)?;
    Ok(RootItem::Memory { regions })
}

fn sections_item(input: &mut &[u8]) -> PResult<RootItem> {
    let _ = ("SECTIONS", wsc!("{")).parse_next(input)?;
    let sections = repeat(1.., wsc!(section_command)).parse_next(input)?;
    let _ = wsc!("}").parse_next(input)?;
    Ok(RootItem::Sections { list: sections })
}

fn root_item(input: &mut &[u8]) -> PResult<RootItem> {
    alt((
        early_command_item,
        memory_item,
        statement_item,
        sections_item,
        late_command_item,
    ))
    .parse_next(input)
}

pub fn parse(input: &mut &[u8]) -> PResult<Vec<RootItem>> {
    alt((repeat(1.., wsc!(root_item)), opt_space.map(|_| vec![]))).parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::{self};

    proptest! {
        #[test]
        fn proptest_root_items(value in arb_root_item()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = root_item.parse(&v);
            assert_eq!(r, Ok(value));
        }
    }

    #[test]
    fn test_empty() {
        assert_done_vec!(parse.parse(b""), 0);
        assert_done_vec!(parse.parse(b"                               "), 0);
        assert_done_vec!(parse.parse(b"      /* hello */              "), 0);
    }

    #[test]
    fn test_parse() {
        for entry in fs::read_dir("tests").unwrap() {
            let path = entry.unwrap().path();
            println!("testing: {:?}", path);
            let contents: Vec<u8> = std::fs::read(&path).unwrap();
            println!("{:?}", contents.len());
            let parsed = parse.parse(&contents);
            assert_done!(parsed);
            let parsed = parsed.unwrap();
            let mut ctx = LdCtx::new(vec![]);
            for v in &parsed {
                v.ld_write(&mut ctx).unwrap();
            }
            assert_eq!(ctx.indent, 0);

            std::fs::write("test_out.log", &ctx.write).unwrap();
            let parsed2 = parse.parse(&ctx.write);
            assert!(parsed2.is_ok());
            assert_eq!(parsed, parsed2.unwrap());
        }
    }
}
