#[allow(unused_macros)]
macro_rules! assert_done {
    ($res:expr) => {{
        assert!($res.is_ok(), "Unexpected failure: {:?}", $res);
    }};
}

#[allow(unused_macros)]
macro_rules! assert_done_vec {
    ($res:expr, $num:expr) => {{
        assert!($res.is_ok(), "Fail: {:?}", $res);
        assert_eq!($res.unwrap().len(), $num, "Unexpected Length");
    }};
}

#[allow(unused_macros)]
macro_rules! assert_fail {
    ($res:expr) => {{
        assert!($res.is_err(), "Expected failure: Got {:?}", $res);
    }};
}
