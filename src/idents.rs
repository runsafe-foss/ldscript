use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;

use std::borrow::Cow;
use std::io::Write;
use winnow::combinator::{alt, delimited, opt};
use winnow::prelude::*;
use winnow::token::{any as any_tok, take_till, take_until0, take_while};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct LdString<'a>(Cow<'a, [u8]>);

#[cfg(test)]
pub fn arb_ldstring() -> impl Strategy<Value = LdString<'static>> {
    arb_string().prop_map(|v| LdString(v.into()))
}

impl<W> LdWrite<W> for LdString<'_>
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        write!(ctx, "\"")?;
        ctx.write_literal(self.0.as_ref())?;
        write!(ctx, "\"")?;
        Ok(())
    }
}

impl LdString<'_> {
    pub fn into_owned(&self) -> LdString<'static> {
        LdString(self.0.clone().into_owned().into())
    }
}

impl<'a> From<&'a str> for LdString<'a> {
    fn from(s: &'a str) -> Self {
        LdString(s.as_bytes().into())
    }
}

impl<'a> TryFrom<&'a [u8]> for LdString<'a> {
    type Error = ();
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        quoted_string.parse(s).map_err(|_| ())
    }
}

#[cfg(test)]
pub fn arb_string() -> impl Strategy<Value = Vec<u8>> {
    proptest::string::bytes_regex(r#"[^\\"]*"#).unwrap()
}

#[cfg(test)]
fn is_keyword(keyword: &[u8]) -> bool {
    use std::collections::HashSet;
    use std::sync::OnceLock;
    static KEYWORDS: OnceLock<HashSet<&[u8]>> = OnceLock::new();

    let keywords = KEYWORDS.get_or_init(|| {
        let keywords: &[&str] = &[
            "AFTER",
            "ALIGN",
            "ALIGN_WITH_INPUT",
            "ASSERT",
            "AS_NEEDED",
            "AT",
            "BEFORE",
            "BYTE",
            "EXCLUDE_FILE",
            "FILL",
            "FORCE_COMMON_ALLOCATION",
            "GROUP",
            "HIDDEN",
            "INCLUDE",
            "INPUT",
            "INSERT",
            "KEEP",
            "LENGTH",
            "LONG",
            "MEMORY",
            "NOCROSSREFS",
            "ONLY_IF_RO",
            "ONLY_IF_RW",
            "ORIGIN",
            "OUTPUT",
            "OUTPUT_ARCH",
            "OUTPUT_FORMAT",
            "PROVIDE",
            "PROVIDE_HIDDEN",
            "QUAD",
            "SEARCH_DIR",
            "SECTIONS",
            "SHORT",
            "SORT_BY_ALIGNMENT",
            "SORT_BY_INIT_PRIORITY",
            "SORT_BY_NAME",
            "SORT_NONE",
            "STARTUP",
            "SUBALIGN",
            "TARGET",
        ];
        keywords.into_iter().map(|s| s.as_bytes()).collect()
    });
    keywords.get(keyword).is_some() || keywords.iter().find(|k| keyword.starts_with(k)).is_some()
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum LdFile<'a> {
    /// No Prefix
    Plain(LdFilePath<'a>),
    /// Starts with '=',
    PrefixSysroot(LdFilePath<'a>),
    /// Starts with '-l'
    LibrarySearch(LdFilePath<'a>),
}

impl<W> LdWrite<W> for LdFile<'_>
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let (prefix, path) = match self {
            Self::Plain(p) => ("", p),
            Self::PrefixSysroot(p) => ("=", p),
            Self::LibrarySearch(p) => ("-l", p),
        };

        let needs_quotes = (prefix != "" && ctx.quote_file)
            || ld_file_path_ident.parse(&mut &path.as_ref()).is_err()
            || path.as_ref().starts_with(b"/*");
        if needs_quotes {
            write!(ctx, "\"")?;
        }
        ctx.write_literal(prefix.as_ref())?;
        ctx.write_literal(path.as_ref())?;
        if needs_quotes {
            write!(ctx, "\"")?;
        }
        Ok(())
    }
}

#[cfg(test)]
pub fn arb_ld_file() -> impl Strategy<Value = LdFile<'static>> {
    prop_oneof![
        arb_ld_file_path().prop_map(|p| LdFile::Plain(p.into())),
        arb_ld_file_path().prop_map(|p| LdFile::PrefixSysroot(p.into())),
        arb_ld_file_path().prop_map(|p| LdFile::LibrarySearch(p.into())),
    ]
}

impl<'a> LdFile<'a> {
    pub fn path(&self) -> &LdFilePath<'a> {
        match self {
            Self::Plain(p) => p,
            Self::PrefixSysroot(p) => p,
            Self::LibrarySearch(p) => p,
        }
    }

    pub fn original(&'a self) -> Cow<'a, [u8]> {
        match self {
            Self::Plain(p) => p.as_ref().into(),
            Self::PrefixSysroot(p) => {
                let mut new = vec![b'='];
                new.extend(p.as_ref());
                new.into()
            }
            Self::LibrarySearch(p) => {
                let mut new = vec![b'-', b'l'];
                new.extend(p.as_ref());
                new.into()
            }
        }
    }
}
impl LdFile<'_> {
    pub fn into_owned(&self) -> LdFile<'static> {
        match self {
            Self::Plain(p) => LdFile::Plain(p.into_owned()),
            Self::PrefixSysroot(p) => LdFile::PrefixSysroot(p.into_owned()),
            Self::LibrarySearch(p) => LdFile::LibrarySearch(p.into_owned()),
        }
    }
}

impl<'a> TryFrom<&'a str> for LdFile<'a> {
    type Error = ();
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        ld_file.parse(s.as_bytes()).map_err(|_| ())
    }
}

impl<'a> TryFrom<&'a [u8]> for LdFile<'a> {
    type Error = ();
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        ld_file.parse(s).map_err(|_| ())
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct LdFilePath<'a>(Cow<'a, [u8]>);

#[cfg(test)]
pub fn arb_ld_file_path() -> impl Strategy<Value = LdFilePath<'static>> {
    prop_oneof![
        //Technically this allows commas but it doesn't make sense in a list context.
        proptest::string::bytes_regex(r"[_a-zA-Z/\.\\$~][_a-zA-Z0-9\/\.\\$~\-+:\[\]=]*").unwrap(),
        arb_string()
    ]
    .prop_filter("Don't generate keywords", |v| !is_keyword(v))
    .prop_filter("Don't create prefixes", |v| {
        !v.starts_with(b"=") && !v.starts_with(b"-l")
    })
    .prop_map(|v| LdFilePath(v.into()))
}

impl<'a> AsRef<[u8]> for LdFilePath<'a> {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl LdFilePath<'_> {
    pub fn into_owned(&self) -> LdFilePath<'static> {
        LdFilePath(self.0.clone().into_owned().into())
    }
}

impl<W> LdWrite<W> for LdFilePath<'_>
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let needs_quotes =
            ld_file_path_ident.parse(&mut &self.0).is_err() || self.0.starts_with(b"/*");
        if needs_quotes {
            LdString(self.0.as_ref().into()).ld_write(ctx)?;
        } else {
            ctx.write_literal(self.0.as_ref())?;
        }

        Ok(())
    }
}

impl<'a> TryFrom<&'a str> for LdFilePath<'a> {
    type Error = ();
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        ld_file_path.parse(s.as_bytes()).map_err(|_| ())
    }
}

impl<'a> TryFrom<&'a [u8]> for LdFilePath<'a> {
    type Error = ();
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        ld_file_path.parse(s).map_err(|_| ())
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Symbol<'a>(Cow<'a, [u8]>);

#[cfg(test)]
pub fn arb_symbol() -> impl Strategy<Value = Symbol<'static>> {
    prop_oneof![
        proptest::string::bytes_regex(r"[a-zA-Z_\.]+[a-zA-Z0-9_\.\-]*").unwrap(),
        arb_string()
    ]
    .prop_filter("Don't generate keywords", |v| !is_keyword(v))
    .prop_map(|v| Symbol(v.into()))
}

impl<'a> AsRef<[u8]> for Symbol<'a> {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl Symbol<'_> {
    pub fn into_owned(&self) -> Symbol<'static> {
        Symbol(self.0.clone().into_owned().into())
    }
}

impl<W> LdWrite<W> for Symbol<'_>
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let needs_quotes = simple.parse(&mut &self.0).is_err();

        if needs_quotes {
            LdString(self.0.as_ref().into()).ld_write(ctx)?;
        } else {
            ctx.write_literal(self.0.as_ref())?;
        }

        Ok(())
    }
}

impl<'a> TryFrom<&'a str> for Symbol<'a> {
    type Error = ();
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        symbol.parse(s.as_bytes()).map_err(|_| ())
    }
}

impl<'a> TryFrom<&'a [u8]> for Symbol<'a> {
    type Error = ();
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        symbol.parse(s).map_err(|_| ())
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Pattern<'a>(Cow<'a, [u8]>);

#[cfg(test)]
pub fn arb_pattern() -> impl Strategy<Value = Pattern<'static>> {
    prop_oneof![
        proptest::string::bytes_regex(r"[a-zA-Z0-9_\.$/\\~=+\[\]*?\-!<>^:]+").unwrap(),
        arb_string(),
    ]
    .prop_filter("Don't generate keywords", |v| !is_keyword(v))
    .prop_map(|v| Pattern(v.into()))
}

impl<'a> AsRef<[u8]> for Pattern<'a> {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl Pattern<'_> {
    pub fn into_owned(&self) -> Pattern<'static> {
        Pattern(self.0.clone().into_owned().into())
    }
}

impl<'a> TryFrom<&'a str> for Pattern<'a> {
    type Error = ();
    fn try_from(s: &'a str) -> Result<Self, Self::Error> {
        pattern.parse(s.as_bytes()).map_err(|_| ())
    }
}

impl<'a> TryFrom<&'a [u8]> for Pattern<'a> {
    type Error = ();
    fn try_from(s: &'a [u8]) -> Result<Self, Self::Error> {
        pattern.parse(s).map_err(|_| ())
    }
}

impl<W> LdWrite<W> for Pattern<'_>
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let needs_quotes = simple_pattern.parse(&mut &self.0).is_err() || self.0.starts_with(b"/*");

        if needs_quotes {
            LdString(self.0.as_ref().into()).ld_write(ctx)?;
        } else {
            ctx.write_literal(self.0.as_ref())?;
        }

        Ok(())
    }
}

pub fn quoted_string<'i>(input: &mut &'i [u8]) -> PResult<LdString<'i>> {
    string.map(|s| LdString(s.into())).parse_next(input)
}

fn string<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    delimited("\"", take_until0("\""), "\"").parse_next(input)
}

fn simple<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    (
        any_tok.verify(|c: &u8| c.is_ascii_alphabetic() || *c == b'_' || *c == b'.'),
        take_till(0.., |c: u8| {
            !(c.is_ascii_alphanumeric() || c == b'_' || c == b'.' || c == b'-')
        }),
    )
        .recognize()
        //.verify(|s| !KEYWORDS.contains(s))
        .parse_next(input)
}

pub fn symbol<'i>(input: &mut &'i [u8]) -> PResult<Symbol<'i>> {
    alt((string, simple))
        .map(|s| Symbol(Cow::from(s)))
        .parse_next(input)
}

fn is_pattern(c: u8) -> bool {
    c.is_ascii_alphanumeric() || b"_.$/\\~=+[]*?-!<>^:".contains(&c)
}

fn simple_pattern<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    take_while(1.., is_pattern).parse_next(input)
}

pub fn pattern<'i>(input: &mut &'i [u8]) -> PResult<Pattern<'i>> {
    alt((string, simple_pattern))
        .map(|s| Pattern(Cow::from(s)))
        .parse_next(input)
}
pub fn ld_file_path_ident<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    (
        any_tok.verify(|c: &u8| c.is_ascii_alphabetic() || br"_/.\\$~".contains(c)),
        take_till(0.., |c: u8| {
            // According to the ll file, this allows commas, but it doesn't really make sense in list context.
            !(c.is_ascii_alphanumeric() || br"_/.\\$~-+:[]=".contains(&c))
        }),
    )
        .recognize()
        .parse_next(input)
}

fn ld_file_path<'i>(input: &mut &'i [u8]) -> PResult<LdFilePath<'i>> {
    alt((string, ld_file_path_ident))
        .map(|s| LdFilePath(Cow::from(s)))
        .parse_next(input)
}

pub fn ld_file<'i>(input: &mut &'i [u8]) -> PResult<LdFile<'i>> {
    alt((
        string,
        ((opt(alt(("=", "-l"))), ld_file_path_ident).recognize()),
    ))
    .map(|s| {
        if let Some(suffix) = s.strip_prefix(b"=") {
            LdFile::PrefixSysroot(LdFilePath(Cow::from(suffix)))
        } else if let Some(suffix) = s.strip_prefix(b"-l") {
            LdFile::LibrarySearch(LdFilePath(Cow::from(suffix)))
        } else {
            LdFile::Plain(LdFilePath(Cow::from(s)))
        }
    })
    .parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    proptest! {
        #[test]
        fn proptest_string(value in arb_ldstring()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = quoted_string.parse(&v);
            assert_eq!(r, Ok(value));
        }
        #[test]
        fn proptest_symbol(value in arb_symbol()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = symbol.parse(&v);
            assert_eq!(r, Ok(value));
        }
        #[test]
        fn proptest_pattern(value in arb_pattern()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = pattern.parse(&v);
            assert_eq!(r, Ok(value));
        }

        #[test]
        fn proptest_ld_file_path(value in arb_ld_file_path()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = ld_file_path.parse(&v);
            assert_eq!(r, Ok(value));
        }

        #[test]
        fn proptest_ld_file(value in arb_ld_file()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r = ld_file.parse(&v);
            assert_eq!(r, Ok(value));
        }
    }
    #[test]
    fn test_symbol() {
        assert_eq!(symbol.parse(b".0"), Ok(Symbol(b".0"[..].into())));
        assert_eq!(symbol.parse(b".text"), Ok(Symbol(b".text"[..].into())));
        assert_eq!(symbol.parse(b"a-b"), Ok(Symbol(b"a-b"[..].into())));
        assert_eq!(symbol.parse(b"ab-"), Ok(Symbol(b"ab-"[..].into())));
        assert_eq!(
            symbol.parse(b"\"spaces are ok, just quote the identifier\""),
            Ok(Symbol(
                b"spaces are ok, just quote the identifier"[..].into()
            ))
        );
    }

    #[test]
    fn test_pattern() {
        assert_eq!(pattern.parse(b"0"), Ok(Pattern(b"0"[..].into())));
        assert_eq!(pattern.parse(b".text"), Ok(Pattern(b".text"[..].into())));
        assert_eq!(
            pattern.parse(b"hello*.o"),
            Ok(Pattern(b"hello*.o"[..].into()))
        );
        assert_eq!(
            pattern.parse(b"\"spaces are ok, just quote the identifier\""),
            Ok(Pattern(
                b"spaces are ok, just quote the identifier"[..].into()
            ))
        );
        assert_eq!(
            pattern.parse(b"this+is-another*crazy[example]"),
            Ok(Pattern(b"this+is-another*crazy[example]"[..].into()))
        );
        assert_eq!(
            pattern.parse(b"/some/file/"),
            Ok(Pattern(b"/some/file/"[..].into()))
        );
    }
}
