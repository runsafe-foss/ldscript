#[cfg(test)]
use crate::expressions::arb_expression;
use crate::expressions::{expression, Expression};
#[cfg(test)]
use crate::idents::{arb_ld_file, arb_symbol};
use crate::idents::{ld_file, symbol, LdFile, Symbol};
use crate::whitespace::space;
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
#[cfg(test)]
use proptest_derive::Arbitrary;
use std::io::Write;
use winnow::combinator::{alt, opt, separated};
use winnow::prelude::*;
use winnow::token::tag;

#[derive(Debug, PartialEq)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum InsertOrder {
    Before,
    After,
}
impl<W> LdWrite<W> for InsertOrder
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let n: &[u8] = match self {
            Self::Before => b"BEFORE",
            Self::After => b"AFTER",
        };
        ctx.write_all(n)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct AsNeeded {
    pub files: Vec<LdFile<'static>>,
}

#[cfg(test)]
pub fn arb_as_needed() -> impl Strategy<Value = AsNeeded> {
    proptest::collection::vec(arb_ld_file(), 1..10).prop_map(|files| AsNeeded { files })
}

impl<W> LdWrite<W> for AsNeeded
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let mut first = true;
        write!(ctx, "AS_NEEDED (")?;
        for a in &self.files {
            if !first {
                write!(ctx, " ")?;
            }
            a.ld_write(ctx)?;
            first = false;
        }
        write!(ctx, ")")?;
        Ok(())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum File {
    Plain(LdFile<'static>),
    AsNeeded(AsNeeded),
}

#[cfg(test)]
pub fn arb_file() -> impl Strategy<Value = File> {
    prop_oneof![
        arb_ld_file().prop_map(File::Plain),
        arb_as_needed().prop_map(File::AsNeeded)
    ]
}

impl<W> LdWrite<W> for File
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            File::Plain(s) => s.ld_write(ctx)?,
            File::AsNeeded(s) => s.ld_write(ctx)?,
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq)]
pub enum Command {
    //Simple { name: String },
    InputCall {
        call: InputCall,
        arguments: Vec<File>,
    },
    SingleFileCall {
        call: SingleFileCall,
        file: LdFile<'static>,
    },
    Call {
        name: Symbol<'static>,
        arguments: Vec<Expression>,
    },
    Include {
        file: LdFile<'static>,
    },
    Insert {
        order: InsertOrder,
        section: LdFile<'static>,
    },
}

#[cfg(test)]
pub fn arb_command() -> impl Strategy<Value = Command> {
    prop_oneof![
        (
            arb_symbol(),
            proptest::collection::vec(arb_expression(), 1..10)
        )
            .prop_map(|(s, args)| {
                Command::Call {
                    name: s,
                    arguments: args,
                }
            }),
        (any::<SingleFileCall>(), arb_ld_file()).prop_map(|(s, args)| {
            Command::SingleFileCall {
                call: s,
                file: args,
            }
        }),
        (
            any::<InputCall>(),
            proptest::collection::vec(arb_file(), 1..10)
        )
            .prop_map(|(s, args)| {
                Command::InputCall {
                    call: s,
                    arguments: args,
                }
            }),
        arb_ld_file().prop_map(|p| { Command::Include { file: p } }),
        (any::<InsertOrder>(), arb_ld_file())
            .prop_map(|(io, section)| { Command::Insert { order: io, section } })
    ]
}
impl<W> LdWrite<W> for Command
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Call { name, arguments } => {
                let mut first = true;
                name.ld_write(ctx)?;

                write!(ctx, "(")?;
                for a in arguments {
                    if !first {
                        write!(ctx, " , ")?;
                    }
                    a.ld_write(ctx)?;
                    first = false;
                }
                write!(ctx, ");")?;
            }
            Self::InputCall { call, arguments } => {
                let mut first = true;
                call.ld_write(ctx)?;

                write!(ctx, "(")?;
                for a in arguments {
                    if !first {
                        if call.need_comma() {
                            write!(ctx, " , ")?;
                        } else {
                            write!(ctx, " ")?;
                        }
                    }
                    a.ld_write(ctx)?;
                    first = false;
                }
                write!(ctx, ");")?;
            }
            Self::SingleFileCall { call, file } => {
                call.ld_write(ctx)?;
                write!(ctx, "(")?;
                file.ld_write(ctx)?;
                write!(ctx, ");")?;
            }
            Self::Include { file } => {
                write!(ctx, "INCLUDE ")?;
                file.ld_write(ctx)?;
                write!(ctx, ";")?;
            }
            Self::Insert { order, section } => {
                write!(ctx, "INSERT ")?;
                order.ld_write(ctx)?;
                write!(ctx, " ")?;
                section.ld_write(ctx)?;
                write!(ctx, ";")?;
            }
        }
        Ok(())
    }
}
fn insert_order(input: &mut &[u8]) -> PResult<InsertOrder> {
    alt((
        "BEFORE".map(|_| InsertOrder::Before),
        "AFTER".map(|_| InsertOrder::After),
    ))
    .parse_next(input)
}

fn as_needed(input: &mut &[u8]) -> PResult<AsNeeded> {
    let _ = "AS_NEEDED".parse_next(input)?;
    let _ = wsc!("(").parse_next(input)?;
    let args = separated(
        1..,
        ld_file.map(|s| s.into_owned()),
        alt((wsc!(","), space)),
    )
    .parse_next(input)?;
    let _ = (opt(space), ")").parse_next(input)?;
    Ok(AsNeeded { files: args })
}

fn file(input: &mut &[u8]) -> PResult<File> {
    alt((
        as_needed.map(File::AsNeeded),
        ld_file.map(|s| File::Plain(s.into_owned())),
    ))
    .parse_next(input)
}

#[cfg_attr(test, derive(Arbitrary))]
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum InputCall {
    Group,
    Input,
    LdFeature,
    Map,
    OutputFormat, // This can technically be 1 or 3 args.
    RegionAlias,
}

impl InputCall {
    fn need_comma(&self) -> bool {
        match self {
            Self::RegionAlias | Self::OutputFormat => true,
            _ => false,
        }
    }
}

impl<W> LdWrite<W> for InputCall
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let b: &[u8] = match self {
            Self::Group => b"GROUP",
            Self::Input => b"INPUT",
            Self::LdFeature => b"LD_FEATURE",
            Self::Map => b"MAP",
            Self::OutputFormat => b"OUTPUT_FORMAT",
            Self::RegionAlias => b"REGION_ALIAS",
        };
        ctx.write_all(b)
    }
}

fn input_call_ident(input: &mut &[u8]) -> PResult<InputCall> {
    alt((
        "GROUP".map(|_| InputCall::Group),
        "INPUT".map(|_| InputCall::Input),
        "LD_FEATURE".map(|_| InputCall::LdFeature),
        "MAP".map(|_| InputCall::Map),
        "OUTPUT_FORMAT".map(|_| InputCall::OutputFormat),
        "REGION_ALIAS".map(|_| InputCall::RegionAlias),
    ))
    .parse_next(input)
}

#[cfg_attr(test, derive(Arbitrary))]
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum SingleFileCall {
    Startup,
    SearchDir,
    Output,
    Target,
    OutputArch,
}

impl<W> LdWrite<W> for SingleFileCall
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let b: &[u8] = match self {
            Self::Startup => b"STARTUP",
            Self::SearchDir => b"SEARCH_DIR",
            Self::Output => b"OUTPUT",
            Self::Target => b"TARGET",
            Self::OutputArch => b"OUTPUT_ARCH",
        };
        ctx.write_all(b)
    }
}

fn single_file_call_ident(input: &mut &[u8]) -> PResult<SingleFileCall> {
    alt((
        "STARTUP".map(|_| SingleFileCall::Startup),
        "SEARCH_DIR".map(|_| SingleFileCall::SearchDir),
        // match longer lexemes first
        "OUTPUT_ARCH".map(|_| SingleFileCall::OutputArch),
        "OUTPUT".map(|_| SingleFileCall::Output),
        "TARGET".map(|_| SingleFileCall::Target),
    ))
    .parse_next(input)
}

fn input_call(input: &mut &[u8]) -> PResult<Command> {
    let call = input_call_ident.parse_next(input)?;
    let _ = wsc!(tag("(")).parse_next(input)?;
    let args = separated(1.., file, alt((wsc!(","), space))).parse_next(input)?;
    let _ = (wsc!(")"), opt(wsc!(";"))).parse_next(input)?;
    Ok(Command::InputCall {
        call,
        arguments: args,
    })
}

fn single_file_call(input: &mut &[u8]) -> PResult<Command> {
    let call = single_file_call_ident.parse_next(input)?;
    let _ = wsc!(tag("(")).parse_next(input)?;
    let file = ld_file.map(|s| s.into_owned()).parse_next(input)?;
    let _ = (wsc!(")"), opt(wsc!(";"))).parse_next(input)?;
    Ok(Command::SingleFileCall { call, file })
}

fn call(input: &mut &[u8]) -> PResult<Command> {
    let name = symbol.map(|s| s.into_owned()).parse_next(input)?;
    let _ = wsc!(tag("(")).parse_next(input)?;
    let args = separated(1.., expression, alt((wsc!(","), space))).parse_next(input)?;
    let _ = (wsc!(")"), opt(wsc!(";"))).parse_next(input)?;
    Ok(Command::Call {
        name: name.into(),
        arguments: args,
    })
}

fn include(input: &mut &[u8]) -> PResult<Command> {
    let _ = ("INCLUDE", space).parse_next(input)?;
    let file = ld_file.map(|s| s.into_owned()).parse_next(input)?;
    let _ = opt(wsc!(";")).parse_next(input)?;
    Ok(Command::Include { file: file.into() })
}

fn insert(input: &mut &[u8]) -> PResult<Command> {
    let _ = ("INSERT", space).parse_next(input)?;
    let order = wsc!(insert_order).parse_next(input)?;
    let section = ld_file.map(|s| s.into_owned()).parse_next(input)?;
    let _ = opt(wsc!(";")).parse_next(input)?;
    Ok(Command::Insert {
        order,
        section: section.into(),
    })
}

pub fn command(input: &mut &[u8]) -> PResult<Command> {
    alt((command_no_call, call)).parse_next(input)
}

/// This function exists because call allows an arbitrary string as it's first token.
/// If it's found first, no statements can match.
/// If statements are first, many of they first tokens here could be confused with Statement::Assign,
pub fn command_no_call(input: &mut &[u8]) -> PResult<Command> {
    alt((include, input_call, single_file_call, insert)).parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    proptest! {
       #[test]
       fn proptest_command(value in arb_command()) {
           let mut v = vec![];
           value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
           let r =  command.parse(&v);
           assert_eq!(r, Ok(value));
       }
    }

    #[test]
    fn test_group() {
        assert_done!(command.parse(b"INPUT(AS_NEEDED(input3 input4) input5);"));
        assert_done!(command.parse(b"GROUP ( /lib/i386-linux-gnu/libc.so.6 );"));
        assert_done!(command.parse(b"GROUP ( /lib/i386-linux-gnu/libc.so.6 /usr/lib/i386-linux-gnu/libc_nonshared.a  AS_NEEDED ( /lib/i386-linux-gnu/ld-linux.so.2 ) )"));
        assert_done!(command.parse(b"GROUP ( /lib/i386-linux-gnu/libc.so.6 , /usr/lib/i386-linux-gnu/libc_nonshared.a ,  AS_NEEDED ( /lib/i386-linux-gnu/ld-linux.so.2 ) )"));
    }

    #[test]
    fn test_command() {
        assert_done!(command.parse(b"FOO((a ? 1 : 2) , 5);"));
        assert_done!(command.parse(b"OUTPUT_FORMAT ( elf32-i386 ) ;"));
        assert_done!(command.parse(b"OUTPUT_ARCH ( 0 ) ;"));
        assert_done!(command.parse(b"OUTPUT_ARCH ( 0 )"));
        assert_done!(command.parse(b"OUTPUT_ARCH ( 0 1 2 )"));
        assert_done!(command.parse(b"OUTPUT_ARCH ( 0, 1 2 )"));
        assert_done!(command.parse(b"OUTPUT_ARCH ( 0, 1, 2 )"));
        assert_done!(command.parse(b"OUTPUT_ARCH(i386:x86-64)"));

        assert_fail!(command.parse(b"OUTPUT_ARCH ( 0, 1, 2, )"));
        assert_fail!(command.parse(b"OUTPUT_ARCH ( )"));

        assert_done!(command.parse(b"INCLUDE abc.h ;"));
        assert_done!(command.parse(b"INCLUDE\tabc.h"));

        assert_done!(command.parse(b"INSERT BEFORE .text  ;"));
        assert_done!(command.parse(b"INSERT  AFTER  .text"));
    }
}
