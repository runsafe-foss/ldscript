#[cfg(test)]
use crate::idents::arb_symbol;
use crate::idents::{symbol, Symbol};
use crate::numbers::number;
use crate::whitespace::opt_space;
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
use std::io::Write;
use winnow::combinator::{alt, delimited, opt};
use winnow::prelude::*;
use winnow::token::take_until0;

#[derive(Debug, PartialEq)]
pub struct Region {
    pub name: Symbol<'static>,
    pub origin: u64,
    pub length: u64,
}

#[cfg(test)]
pub fn arb_region() -> impl Strategy<Value = Region> {
    (arb_symbol(), any::<u64>(), any::<u64>()).prop_map(|(s, o, l)| Region {
        name: s,
        origin: o,
        length: l,
    })
}

impl<W> LdWrite<W> for Region
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        self.name.ld_write(ctx)?;
        //TODO: Missing attributes?
        write!(ctx, " : ORIGIN = {}, LENGTH = {}", self.origin, self.length)?;
        Ok(())
    }
}

fn attributes<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    delimited("(", take_until0(")"), ")").parse_next(input)
}

fn origin<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    alt(("ORIGIN", "org", "o")).parse_next(input)
}

fn length<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
    alt(("LENGTH", "len", "l")).parse_next(input)
}

pub fn region(input: &mut &[u8]) -> PResult<Region> {
    let name = symbol.map(|s| s.into_owned()).parse_next(input)?;
    let _ = (opt_space, opt(attributes), wsc!(":"), origin, wsc!("=")).parse_next(input)?;
    let org = number.parse_next(input)?;
    let _ = ((wsc!(","), length, wsc!("="))).parse_next(input)?;
    let len = number.parse_next(input)?;
    Ok(Region {
        name: name.into(),
        origin: org,
        length: len,
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    proptest! {
        #[test]
        fn proptest_region(value in arb_region()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r =  region.parse(&v);
            assert_eq!(r, Ok(value));
        }
    }

    #[test]
    fn test_region() {
        assert_eq!(
            region.parse(b"rom (rx)  : ORIGIN = 0, LENGTH = 256K"),
            Ok(Region {
                name: "rom".try_into().unwrap(),
                origin: 0,
                length: 256 * 1024,
            })
        );
        assert_eq!(
            region.parse(b"ram (!rx) : org = 0x40000000, l = 4M"),
            Ok(Region {
                name: "ram".try_into().unwrap(),
                origin: 0x40000000,
                length: 4 * 1024 * 1024,
            })
        );
    }
}
