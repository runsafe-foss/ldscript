#[cfg(test)]
use crate::commands::arb_command;
use crate::commands::{command, command_no_call, Command};
#[cfg(test)]
use crate::expressions::arb_expression;
use crate::expressions::expression;
use crate::expressions::Expression;
#[cfg(test)]
use crate::idents::{arb_pattern, arb_symbol};
use crate::idents::{pattern, symbol, Pattern, Symbol};
#[cfg(test)]
use crate::statements::arb_statement;
use crate::statements::{statement, Statement};
use crate::whitespace::opt_space;
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
#[cfg(test)]
use proptest_derive::Arbitrary;
use std::io::Write;
use winnow::combinator::{alt, cut_err, delimited, opt, preceded, repeat};
use winnow::prelude::*;

#[derive(Debug, PartialEq, Clone)]
pub enum SectionSymbol {
    Discard,
    Name(Symbol<'static>),
}

#[cfg(test)]
fn arb_section_symbol() -> impl Strategy<Value = SectionSymbol> {
    prop_oneof![
        Just(SectionSymbol::Discard),
        arb_symbol().prop_map(SectionSymbol::Name)
    ]
}

impl<W> LdWrite<W> for SectionSymbol
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Discard => write!(ctx, "/DISCARD/"),
            Self::Name(n) => n.ld_write(ctx),
        }
    }
}
#[derive(Debug, PartialEq)]
pub enum SectionCommand {
    Statement(Statement),
    Command(Command),
    OutputSection {
        name: SectionSymbol,
        vma_address: Option<Box<Expression>>,
        s_type: Option<OutputSectionType>,
        lma_address: Option<Box<Expression>>,
        section_align: Option<Box<Expression>>,
        align_with_input: bool,
        subsection_align: Option<Box<Expression>>,
        constraint: Option<OutputSectionConstraint>,
        content: Vec<OutputSectionCommand>,
        region: Option<Symbol<'static>>,
        lma_region: Option<Symbol<'static>>,
        fillexp: Option<Box<Expression>>,
    },
}

#[cfg(test)]
pub fn arb_section_command() -> impl Strategy<Value = SectionCommand> {
    use proptest::option;
    prop_oneof![
        arb_statement().prop_map(SectionCommand::Statement),
        arb_command().prop_map(SectionCommand::Command),
        (
            arb_section_symbol(),
            option::of(arb_expression()),
            option::of(any::<OutputSectionType>()),
            option::of(arb_expression()),
            option::of(arb_expression()),
            any::<bool>(),
            option::of(arb_expression()),
            option::of(any::<OutputSectionConstraint>()),
            proptest::collection::vec(arb_output_section_command(), 0..10),
            option::of(arb_symbol()),
            option::of(arb_symbol()),
            option::of(arb_expression()),
        )
            .prop_map(
                |(
                    name,
                    vma_address,
                    s_type,
                    lma_address,
                    section_align,
                    align_with_input,
                    subsection_align,
                    constraint,
                    content,
                    region,
                    lma_region,
                    fillexp,
                )| {
                    SectionCommand::OutputSection {
                        name,
                        vma_address: vma_address.map(Box::new),
                        s_type,
                        lma_address: lma_address.map(Box::new),
                        section_align: section_align.map(Box::new),
                        align_with_input,
                        subsection_align: subsection_align.map(Box::new),
                        constraint,
                        content,
                        region,
                        lma_region,
                        fillexp: fillexp.map(Box::new),
                    }
                }
            )
    ]
}

impl<W> LdWrite<W> for SectionCommand
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            SectionCommand::Statement(s) => {
                s.ld_write(ctx)?;
            }
            SectionCommand::Command(c) => {
                c.ld_write(ctx)?;
            }
            SectionCommand::OutputSection {
                name,
                vma_address,
                s_type,
                lma_address,
                section_align,
                align_with_input,
                subsection_align,
                constraint,
                content,
                region,
                lma_region,
                fillexp,
            } => {
                name.ld_write(ctx)?;
                write!(ctx, " ")?;
                s_type.ld_write(ctx)?;
                write!(ctx, " ")?;
                vma_address.ld_write(ctx)?;
                write!(ctx, " : ")?;
                if let Some(lma) = lma_address {
                    write!(ctx, " AT(")?;
                    lma.ld_write(ctx)?;
                    write!(ctx, ")")?;
                }
                if let Some(align) = section_align {
                    write!(ctx, " ALIGN(")?;
                    align.ld_write(ctx)?;
                    write!(ctx, ")")?;
                }
                if *align_with_input {
                    write!(ctx, " ALIGN_WITH_INPUT")?;
                }
                if let Some(sub) = subsection_align {
                    write!(ctx, " SUBALIGN(")?;
                    sub.ld_write(ctx)?;
                    write!(ctx, ")")?;
                }
                write!(ctx, " ")?;
                constraint.ld_write(ctx)?;

                ctx.with_indent(|ctx| {
                    writeln!(ctx, "{{")?;
                    for c in content {
                        c.ld_write(ctx)?;
                    }
                    Ok(())
                })?;
                writeln!(ctx, "}}")?;
                if let Some(region) = region {
                    write!(ctx, " > ")?;
                    region.ld_write(ctx)?;
                }

                if let Some(region) = lma_region {
                    write!(ctx, " AT> ")?;
                    region.ld_write(ctx)?;
                }

                if let Some(fill) = fillexp {
                    write!(ctx, " = ")?;
                    fill.ld_write(ctx)?;
                }
                writeln!(ctx, ",")?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum OutputSectionCommand {
    Statement(Statement),
    Fill {
        expr: Box<Expression>,
    },
    Data {
        d_type: DataType,
        value: Box<Expression>,
    },
    InputSection {
        file: SectionPattern,
        sections: Vec<SectionPattern>,
    },
    KeepInputSection {
        file: SectionPattern,
        sections: Vec<SectionPattern>,
    },
}

#[cfg(test)]
fn arb_output_section_command() -> impl Strategy<Value = OutputSectionCommand> {
    prop_oneof![
        arb_statement().prop_map(OutputSectionCommand::Statement),
        arb_expression().prop_map(|expr| {
            OutputSectionCommand::Fill {
                expr: Box::new(expr),
            }
        }),
        (any::<DataType>(), arb_expression()).prop_map(|(d_type, expr)| {
            OutputSectionCommand::Data {
                d_type,
                value: Box::new(expr),
            }
        }),
        (
            arb_section_pattern(),
            proptest::collection::vec(arb_section_pattern(), 1..10)
        )
            .prop_map(|(file, sections)| { OutputSectionCommand::InputSection { file, sections } }),
        (
            arb_section_pattern(),
            proptest::collection::vec(arb_section_pattern(), 1..10)
        )
            .prop_map(|(file, sections)| {
                OutputSectionCommand::KeepInputSection { file, sections }
            }),
    ]
}

impl<W> LdWrite<W> for OutputSectionCommand
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Statement(s) => {
                s.ld_write(ctx)?;
            }
            Self::Fill { expr } => {
                write!(ctx, "FILL(")?;
                expr.ld_write(ctx)?;
                writeln!(ctx, ");")?;
            }
            Self::Data { d_type, value } => {
                d_type.ld_write(ctx)?;
                write!(ctx, " (")?;
                value.ld_write(ctx)?;
                writeln!(ctx, ");")?;
            }
            Self::InputSection { file, sections } => {
                file.ld_write(ctx)?;
                if !sections.is_empty() {
                    write!(ctx, "(")?;
                    for s in sections {
                        s.ld_write(ctx)?;
                        write!(ctx, " ")?;
                    }
                    writeln!(ctx, ")")?;
                }
            }
            Self::KeepInputSection { file, sections } => {
                write!(ctx, "KEEP(")?;
                file.ld_write(ctx)?;
                if !sections.is_empty() {
                    write!(ctx, "(")?;
                    for s in sections {
                        s.ld_write(ctx)?;
                        write!(ctx, " ")?;
                    }
                    write!(ctx, ")")?;
                }
                writeln!(ctx, ")")?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum DataType {
    Byte,
    Short,
    Long,
    Quad,
}

impl<W> LdWrite<W> for DataType
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let t: &[u8] = match self {
            DataType::Byte => b"BYTE",
            DataType::Short => b"SHORT",
            DataType::Long => b"LONG",
            DataType::Quad => b"QUAD",
        };
        ctx.write_all(t)
    }
}
#[derive(Debug, PartialEq, Clone)]
pub enum SectionPattern {
    Simple(Pattern<'static>),
    SortByName(Pattern<'static>),
    SortByAlignment(Pattern<'static>),
    SortByInitPriority(Pattern<'static>),
    SortNone(Pattern<'static>),
    ExcludeFile {
        files: Vec<Pattern<'static>>,
        pattern: Box<SectionPattern>,
    },
}

#[cfg(test)]
fn arb_section_pattern() -> impl Strategy<Value = SectionPattern> {
    let leaf = prop_oneof![
        arb_pattern().prop_map(SectionPattern::Simple),
        arb_pattern().prop_map(SectionPattern::SortByName),
        arb_pattern().prop_map(SectionPattern::SortByAlignment),
        arb_pattern().prop_map(SectionPattern::SortByInitPriority),
        arb_pattern().prop_map(SectionPattern::SortNone),
    ];

    leaf.prop_recursive(8, 256, 10, |inner| {
        (proptest::collection::vec(arb_pattern(), 1..10), inner).prop_map(|(files, pattern)| {
            SectionPattern::ExcludeFile {
                files,
                pattern: Box::new(pattern),
            }
        })
    })
}

impl<W> LdWrite<W> for SectionPattern
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Simple(v) => {
                v.ld_write(ctx)?;
            }
            Self::SortByName(v) => {
                write!(ctx, "SORT_BY_NAME(")?;
                v.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::SortByAlignment(v) => {
                write!(ctx, "SORT_BY_ALIGNMENT(")?;
                v.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::SortByInitPriority(v) => {
                write!(ctx, "SORT_BY_INIT_PRIORITY(")?;
                v.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::SortNone(v) => {
                write!(ctx, "SORT_NONE(")?;
                v.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::ExcludeFile { files, pattern } => {
                write!(ctx, "EXCLUDE_FILE(")?;
                for f in files {
                    f.ld_write(ctx)?;
                    write!(ctx, " ")?;
                }
                write!(ctx, ") ")?;
                pattern.ld_write(ctx)?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum OutputSectionType {
    NoLoad,
    DSect,
    Copy,
    Info,
    Overlay,
}

impl<W> LdWrite<W> for OutputSectionType
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let name: &[u8] = match self {
            Self::NoLoad => b"(NOLOAD)",
            Self::DSect => b"(DSECT)",
            Self::Copy => b"(COPY)",
            Self::Info => b"(INFO)",
            Self::Overlay => b"(OVERLAY)",
        };
        ctx.write_all(name)
    }
}

#[derive(Debug, PartialEq)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum OutputSectionConstraint {
    OnlyIfRo,
    OnlyIfRw,
}
impl<W> LdWrite<W> for OutputSectionConstraint
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        let n: &[u8] = match self {
            Self::OnlyIfRo => b"ONLY_IF_RO",
            Self::OnlyIfRw => b"ONLY_IF_RW",
        };
        ctx.write_all(n)
    }
}

fn output_section_type(input: &mut &[u8]) -> PResult<OutputSectionType> {
    alt((
        "(NOLOAD)".map(|_| OutputSectionType::NoLoad),
        "(DSECT)".map(|_| OutputSectionType::DSect),
        "(COPY)".map(|_| OutputSectionType::Copy),
        "(INFO)".map(|_| OutputSectionType::Info),
        "(OVERLAY)".map(|_| OutputSectionType::Overlay),
    ))
    .parse_next(input)
}

fn output_section_constraint(input: &mut &[u8]) -> PResult<OutputSectionConstraint> {
    alt((
        "ONLY_IF_RO".map(|_| OutputSectionConstraint::OnlyIfRo),
        "ONLY_IF_RW".map(|_| OutputSectionConstraint::OnlyIfRw),
    ))
    .parse_next(input)
}

fn sorted_sp(input: &mut &[u8]) -> PResult<SectionPattern> {
    let keyword = alt((
        "SORT_BY_NAME",
        "SORT_BY_ALIGNMENT",
        "SORT_BY_INIT_PRIORITY",
        "SORT_NONE",
        "SORT",
    ))
    .parse_next(input)?;
    let _ = cut_err(wsc!("(")).parse_next(input)?;
    let inner = cut_err(pattern.map(|s| s.into_owned())).parse_next(input)?;
    let _ = cut_err(wsc!(")")).parse_next(input)?;
    Ok(match keyword {
        b"SORT" | b"SORT_BY_NAME" => SectionPattern::SortByName(inner.into()),
        b"SORT_BY_ALIGNMENT" => SectionPattern::SortByAlignment(inner.into()),
        b"SORT_BY_INIT_PRIORITY" => SectionPattern::SortByInitPriority(inner.into()),
        b"SORT_NONE" => SectionPattern::SortNone(inner.into()),
        _ => panic!("wrong sort keyword"),
    })
}

fn exclude_file_sp(input: &mut &[u8]) -> PResult<SectionPattern> {
    let _ = ("EXCLUDE_FILE", wsc!("(")).parse_next(input)?;
    let files = cut_err(repeat(1.., wsc!(pattern.map(|p| p.into_owned())))).parse_next(input)?;
    let _ = cut_err(wsc!(")")).parse_next(input)?;
    let inner = cut_err(section_pattern).parse_next(input)?;
    Ok(SectionPattern::ExcludeFile {
        files,
        pattern: Box::new(inner),
    })
}

fn simple_sp(input: &mut &[u8]) -> PResult<SectionPattern> {
    pattern
        .map(|s| s.into_owned())
        .map(|x| SectionPattern::Simple(x.into()))
        .parse_next(input)
}

fn section_pattern(input: &mut &[u8]) -> PResult<SectionPattern> {
    alt((exclude_file_sp, sorted_sp, simple_sp)).parse_next(input)
}

fn data_osc(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    let d_type = alt(("BYTE", "SHORT", "LONG", "QUAD")).parse_next(input)?;
    let _ = wsc!("(").parse_next(input)?;
    let value = expression.parse_next(input)?;
    let _ = (wsc!(")"), wsc!(opt(";"))).parse_next(input)?;
    Ok(OutputSectionCommand::Data {
        d_type: match d_type {
            b"BYTE" => DataType::Byte,
            b"SHORT" => DataType::Short,
            b"LONG" => DataType::Long,
            b"QUAD" => DataType::Quad,
            _ => panic!("invalid data type"),
        },
        value: Box::new(value),
    })
}

fn fill_osc(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    let _ = (("FILL", wsc!("("))).parse_next(input)?;
    let expr = expression.parse_next(input)?;
    let _ = (wsc!(")"), wsc!(opt(";"))).parse_next(input)?;
    Ok(OutputSectionCommand::Fill {
        expr: Box::new(expr),
    })
}

fn statement_osc(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    statement
        .map(|stmt| OutputSectionCommand::Statement(stmt))
        .parse_next(input)
}

fn input_osc(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    let file = section_pattern(input)?;
    let _ = opt_space(input)?;
    let sections = opt(delimited(
        wsc!("("),
        repeat(1.., wsc!(section_pattern)),
        wsc!(")"),
    ))
    .parse_next(input)?;
    Ok(OutputSectionCommand::InputSection {
        file,
        sections: match sections {
            Some(s) => s,
            None => Vec::new(),
        },
    })
}

fn keep_osc(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    let _ = ("KEEP", wsc!("(")).parse_next(input)?;
    let inner = input_osc.parse_next(input)?;
    let _ = wsc!(")").parse_next(input)?;
    Ok(match inner {
        OutputSectionCommand::InputSection { file, sections } => {
            OutputSectionCommand::KeepInputSection { file, sections }
        }
        _ => panic!("wrong output section command"),
    })
}

fn output_section_command(input: &mut &[u8]) -> PResult<OutputSectionCommand> {
    alt((statement_osc, keep_osc, data_osc, fill_osc, input_osc)).parse_next(input)
}

fn statement_sc(input: &mut &[u8]) -> PResult<SectionCommand> {
    statement
        .map(|stmt| SectionCommand::Statement(stmt))
        .parse_next(input)
}

fn command_sc(input: &mut &[u8]) -> PResult<SectionCommand> {
    command
        .map(|cmd| SectionCommand::Command(cmd))
        .parse_next(input)
}

fn early_command_sc(input: &mut &[u8]) -> PResult<SectionCommand> {
    command_no_call
        .map(|cmd| SectionCommand::Command(cmd))
        .parse_next(input)
}

fn section_symbol(input: &mut &[u8]) -> PResult<SectionSymbol> {
    alt((
        "/DISCARD/".map(|_| SectionSymbol::Discard),
        symbol.map(|s| s.into_owned()).map(SectionSymbol::Name),
    ))
    .parse_next(input)
}

fn output_sc(input: &mut &[u8]) -> PResult<SectionCommand> {
    let name = section_symbol.parse_next(input)?;
    let _ = opt_space.parse_next(input)?;
    let s_type1 = opt(output_section_type).parse_next(input)?;
    let vma = wsc!(opt(expression)).parse_next(input)?;
    let s_type2 = opt(output_section_type).parse_next(input)?;
    let _ = wsc!(":").parse_next(input)?;
    let lma = opt(delimited(("AT", wsc!("(")), wsc!(expression), ")")).parse_next(input)?;
    let _ = opt_space.parse_next(input)?;
    let section_align =
        opt(delimited(("ALIGN", wsc!("(")), wsc!(expression), ")")).parse_next(input)?;
    let align_with_input = wsc!(opt("ALIGN_WITH_INPUT")).parse_next(input)?;
    let subsection_align =
        opt(delimited(("SUBALIGN", wsc!("(")), wsc!(expression), ")")).parse_next(input)?;
    let constraint = wsc!(opt(output_section_constraint)).parse_next(input)?;
    let _ = wsc!("{").parse_next(input)?;
    let content = repeat(0.., wsc!(output_section_command)).parse_next(input)?;
    let _ = wsc!("}").parse_next(input)?;
    let region = opt(preceded(">", wsc!(symbol.map(|s| s.into_owned())))).parse_next(input)?;
    let lma_region =
        opt(preceded("AT>", wsc!(symbol.map(|s| s.into_owned())))).parse_next(input)?;
    let fillexp = opt(preceded("=", wsc!(expression))).parse_next(input)?;
    let _ = wsc!(opt(",")).parse_next(input)?;
    Ok(SectionCommand::OutputSection {
        name: name.into(),
        vma_address: vma.map(Box::new),
        s_type: if s_type1.is_some() { s_type1 } else { s_type2 },
        lma_address: lma.map(Box::new),
        section_align: section_align.map(Box::new),
        align_with_input: align_with_input.is_some(),
        subsection_align: subsection_align.map(Box::new),
        constraint,
        content,
        region: region.map(Into::into),
        lma_region: lma_region.map(Into::into),
        fillexp: fillexp.map(Box::new),
    })
}

pub fn section_command(input: &mut &[u8]) -> PResult<SectionCommand> {
    alt((early_command_sc, output_sc, statement_sc, command_sc)).parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    proptest! {
       #[test]
       fn proptest_section_symbol(value in arb_section_symbol()) {
           let mut v = vec![];
           value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
           let r =  section_symbol.parse(&v);
           assert_eq!(r, Ok(value));
       }
       #[test]
       fn proptest_section_command(value in arb_section_command()) {
           let mut v = vec![];
           value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
           let r =  section_command.parse(&v);
           assert_eq!(r, Ok(value));
       }

       #[test]
       fn proptest_section_pattern(value in arb_section_pattern()) {
           let mut v = vec![];
           value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
           let r =  section_pattern.parse(&v);
           assert_eq!(r, Ok(value));
       }
       #[test]
       fn proptest_output_section_command(value in arb_output_section_command()) {
           let mut v = vec![];
           value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
           let r = output_section_command.parse(&v);
           assert_eq!(r, Ok(value));
       }
    }

    #[test]
    fn test_section_command() {
        assert_fail!(section_pattern.parse(b"EXCLUDE_FILE (*a)"));
        assert_fail!(input_osc.parse(b"EXCLUDE_FILE (*a)"));
        assert_done!(section_pattern.parse(b"EXCLUDE_FILE ( *a *b ) .c"));
        assert_done!(input_osc.parse(b"EXCLUDE_FILE ( *a *b ) *c"));

        assert_fail!(input_osc.parse(b"EXCLUDE_FILE ( EXCLUDE_FILE ( *a *b ) *c ) .d"));
        assert_done!(input_osc.parse(b"EXCLUDE_FILE ( *a ) *b ( .c )"));
        assert_done!(input_osc.parse(b"EXCLUDE_FILE ( *a ) *b ( .c .d )"));
        assert_done!(input_osc.parse(b"EXCLUDE_FILE ( *a ) *b ( .c EXCLUDE_FILE ( *a ) .d )",));

        assert_done!(output_section_command.parse(b"[A-Z]*(.data)"));
        assert_done!(output_section_command.parse(b"LONG((__CTOR_END__ - __CTOR_LIST__) / 4 - 2)",));
        assert_done!(
            output_section_command.parse(b"EXCLUDE_FILE (*crtend.o *otherfile.o) *(.ctors)",)
        );
        assert_done!(
            output_section_command.parse(b"*(EXCLUDE_FILE (*crtend.o *otherfile.o) .ctors)",)
        );
        assert_done!(
            output_section_command.parse(b"*(EXCLUDE_FILE (*a) .text EXCLUDE_FILE (*b) .c)",)
        );
        assert_done!(output_section_command.parse(b"KEEP(SORT_BY_NAME(*)(.ctors))"));
        assert_done!(output_section_command.parse(b"PROVIDE (__init_array_end = .);"));
        assert_done!(output_section_command.parse(b"LONG(0);"));
        assert_done!(output_section_command.parse(b"SORT(CONSTRUCTORS)"));
        assert_done!(output_section_command.parse(b"*"));

        assert_done!(statement_osc.parse(b"ASSERT(SIZEOF(.upper)==0,\"Test\");"));
        assert_done!(output_section_command.parse(b"ASSERT(SIZEOF(.upper)==0,\"Test\");",));
        assert_done!(output_section_command.parse(b"FILL(0xff);"));

        assert_done!(output_sc.parse(b"/DISCARD/ : { *(.note.GNU-stack) }"));
        assert_done!(output_sc.parse(b".DATA : { [A-Z]*(.data) }"));
        assert_done!(output_sc.parse(b".infoD     : {} > INFOD"));

        assert_done!(output_sc.parse(b".a:{*(.b .c)*(.d .e)}"));
    }
}
