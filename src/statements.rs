#[cfg(test)]
use crate::expressions::arb_expression;
use crate::expressions::expression;
use crate::expressions::Expression;
#[cfg(test)]
use crate::idents::{arb_ldstring, arb_symbol};
use crate::idents::{quoted_string, symbol, LdString, Symbol};
use crate::{LdCtx, LdWrite};
#[cfg(test)]
use proptest::prelude::*;
#[cfg(test)]
use proptest_derive::Arbitrary;
use std::io::Write;
use winnow::combinator::{alt, opt};
use winnow::prelude::*;

#[derive(Debug, PartialEq, Clone)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum AssignOperator {
    Equals,
    Plus,
    Minus,
    Multiply,
    Divide,
    ShiftLeft,
    ShiftRight,
    And,
    Or,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Assign {
        name: Symbol<'static>,
        operator: AssignOperator,
        expression: Box<Expression>,
    },
    Hidden {
        name: Symbol<'static>,
        expression: Box<Expression>,
    },
    Provide {
        name: Symbol<'static>,
        expression: Box<Expression>,
    },
    ProvideHidden {
        name: Symbol<'static>,
        expression: Box<Expression>,
    },
    Assert {
        expr: Box<Expression>,
        text: LdString<'static>,
    },
}
#[cfg(test)]
pub fn arb_statement() -> impl Strategy<Value = Statement> {
    prop_oneof![
        (arb_symbol(), any::<AssignOperator>(), arb_expression()).prop_map(|(s, o, e)| {
            Statement::Assign {
                name: s,
                operator: o,
                expression: Box::new(e),
            }
        }),
        (arb_symbol(), arb_expression()).prop_map(|(s, e)| {
            Statement::Hidden {
                name: s,
                expression: Box::new(e),
            }
        }),
        (arb_symbol(), arb_expression()).prop_map(|(s, e)| {
            Statement::Provide {
                name: s,
                expression: Box::new(e),
            }
        }),
        (arb_symbol(), arb_expression()).prop_map(|(s, e)| {
            Statement::ProvideHidden {
                name: s,
                expression: Box::new(e),
            }
        }),
        (arb_ldstring(), arb_expression()).prop_map(|(s, e)| {
            Statement::Assert {
                text: s,
                expr: Box::new(e),
            }
        }),
    ]
}

impl<W> LdWrite<W> for Statement
where
    W: Write,
{
    fn ld_write(&self, ctx: &mut LdCtx<W>) -> std::io::Result<()> {
        match self {
            Self::Assign {
                name,
                operator,
                expression,
            } => {
                name.ld_write(ctx)?;
                write!(ctx, " ")?;
                let op: &[u8] = match operator {
                    AssignOperator::Equals => b"=",
                    AssignOperator::Plus => b"+=",
                    AssignOperator::Minus => b"-=",
                    AssignOperator::Multiply => b"*=",
                    AssignOperator::Divide => b"/=",
                    AssignOperator::ShiftLeft => b"<<=",
                    AssignOperator::ShiftRight => b">>=",
                    AssignOperator::And => b"&=",
                    AssignOperator::Or => b"|=",
                };
                ctx.write_all(op)?;
                write!(ctx, " ")?;
                expression.ld_write(ctx)?;
            }
            Self::Hidden { name, expression } => {
                write!(ctx, "HIDDEN (")?;
                name.ld_write(ctx)?;
                write!(ctx, " = ")?;
                expression.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::Provide { name, expression } => {
                write!(ctx, "PROVIDE (")?;
                name.ld_write(ctx)?;
                write!(ctx, " = ")?;
                expression.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::ProvideHidden { name, expression } => {
                write!(ctx, "PROVIDE_HIDDEN (")?;
                name.ld_write(ctx)?;
                write!(ctx, " = ")?;
                expression.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
            Self::Assert { expr, text } => {
                write!(ctx, "ASSERT (")?;
                expr.ld_write(ctx)?;
                write!(ctx, " , ")?;
                text.ld_write(ctx)?;
                write!(ctx, ")")?;
            }
        }
        writeln!(ctx, ";")?;
        Ok(())
    }
}

fn assign_operator(input: &mut &[u8]) -> PResult<AssignOperator> {
    alt((
        b"=", b"+=", b"-=", b"*=", b"/=", b"<<=", b">>=", b"&=", b"|=",
    ))
    .map(|op: &[u8]| match op {
        b"=" => AssignOperator::Equals,
        b"+=" => AssignOperator::Plus,
        b"-=" => AssignOperator::Minus,
        b"*=" => AssignOperator::Multiply,
        b"/=" => AssignOperator::Divide,
        b"<<=" => AssignOperator::ShiftLeft,
        b">>=" => AssignOperator::ShiftRight,
        b"&=" => AssignOperator::And,
        b"|=" => AssignOperator::Or,
        _ => panic!("wrong operator"),
    })
    .parse_next(input)
}

fn special_assign(input: &mut &[u8]) -> PResult<Statement> {
    let keyword = alt(("PROVIDE_HIDDEN", "PROVIDE", "HIDDEN")).parse_next(input)?;
    let _ = wsc!("(").parse_next(input)?;
    let name = symbol.map(|s| s.into_owned()).parse_next(input)?;
    let _ = wsc!("=").parse_next(input)?;
    let expr = expression.parse_next(input)?;
    let _ = wsc!(")").parse_next(input)?;
    let _ = wsc!(";").parse_next(input)?;
    Ok(match keyword {
        b"HIDDEN" => Statement::Hidden {
            name: name.into(),
            expression: Box::new(expr),
        },
        b"PROVIDE" => Statement::Provide {
            name: name.into(),
            expression: Box::new(expr),
        },
        b"PROVIDE_HIDDEN" => Statement::ProvideHidden {
            name: name.into(),
            expression: Box::new(expr),
        },
        _ => panic!("invalid assign keyword"),
    })
}

fn assign(input: &mut &[u8]) -> PResult<Statement> {
    let name = symbol.map(|s| s.into_owned()).parse_next(input)?;
    let op = wsc!(assign_operator).parse_next(input)?;
    let expr = expression.parse_next(input)?;
    let _ = wsc!(";").parse_next(input)?;
    Ok(Statement::Assign {
        name: name.into(),
        operator: op,
        expression: Box::new(expr),
    })
}

fn assert_stmt(input: &mut &[u8]) -> PResult<Statement> {
    let _ = "ASSERT".parse_next(input)?;
    let _ = wsc!("(").parse_next(input)?;
    let expr = expression.parse_next(input)?;
    let _ = wsc!(",").parse_next(input)?;
    let text = quoted_string.map(|s| s.into_owned()).parse_next(input)?;
    let _ = wsc!(")").parse_next(input)?;
    let _ = opt(wsc!(";")).parse_next(input)?;
    Ok(Statement::Assert {
        expr: Box::new(expr),
        text: text.into(),
    })
}

pub fn statement(input: &mut &[u8]) -> PResult<Statement> {
    alt((special_assign, assign, assert_stmt)).parse_next(input)
}

#[cfg(test)]
mod tests {
    use super::*;
    proptest! {
        #[test]
        fn proptest_statement(value in arb_statement()) {
            let mut v = vec![];
            value.ld_write(&mut LdCtx::new(&mut v)).unwrap();
            let r =  statement.parse(&v);
            assert_eq!(r, Ok(value));
        }
    }
    #[test]
    fn test_statement() {
        assert_eq!(
            statement.parse(b"A = 11 ;"),
            Ok(Statement::Assign {
                name: "A".try_into().unwrap(),
                operator: AssignOperator::Equals,
                expression: Box::new(Expression::Number(11)),
            })
        );
        assert_eq!(
            statement.parse(b"PROVIDE ( x = x ) ;"),
            Ok(Statement::Provide {
                name: "x".try_into().unwrap(),
                expression: Box::new(Expression::Ident("x".try_into().unwrap())),
            })
        );
        assert_done!(statement.parse(b"PROBLEM += HELLO ( WORLD , 0 ) + 1 ;"));
    }
}
