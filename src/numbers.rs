use std::panic;

#[cfg(test)]
use proptest::prelude::*;
use winnow::ascii::hex_digit1;
use winnow::combinator::{alt, not, opt};
use winnow::prelude::*;
use winnow::token::{one_of, take_while};

fn mul_suffix0(input: &mut &[u8]) -> PResult<u64> {
    opt(one_of(['k', 'K', 'm', 'M']).map(|c| match c {
        b'k' | b'K' => 1024,
        b'm' | b'M' => 1024 * 1024,
        _ => panic!("Should not happen"),
    }))
    .map(|c| c.unwrap_or(1))
    .parse_next(input)
}

fn prefixed_hex(input: &mut &[u8]) -> PResult<u64> {
    let _ = alt(("0x", "0X")).parse_next(input)?;
    let num = hex_digit1
        .try_map(|num| std::str::from_utf8(num))
        .try_map(|num| u64::from_str_radix(num, 16))
        .parse_next(input)?;

    let mul = mul_suffix0
        .verify_map(|mul| num.checked_mul(mul))
        .parse_next(input)?;

    let _ = not(winnow::ascii::alphanumeric1).parse_peek(*input)?;
    Ok(mul)
}

fn is_num_or_suffix(c: u8) -> bool {
    match c {
        b'0'..=b'9'
        | b'A'..=b'F'
        | b'a'..=b'f'
        | b'h'
        | b'H'
        | b'o'
        | b'O'
        | b'k'
        | b'K'
        | b'm'
        | b'M' => true,
        _ => false,
    }
}

fn parse_oct_or_dec(num: &str) -> Result<u64, std::num::ParseIntError> {
    let radix = match num.chars().next() {
        Some('0') => 8,
        _ => 10,
    };
    u64::from_str_radix(num, radix)
}

fn suffixed_num(input: &mut &[u8]) -> PResult<u64> {
    take_while(1.., is_num_or_suffix)
        .try_map(|num| std::str::from_utf8(num))
        .try_map(|num| match num.char_indices().last() {
            Some((0, _)) => u64::from_str_radix(num, 10),
            Some((n, 'b')) | Some((n, 'B')) => u64::from_str_radix(&num[..n], 2),
            Some((n, 'o')) | Some((n, 'O')) => u64::from_str_radix(&num[..n], 8),
            Some((n, 'd')) | Some((n, 'D')) => u64::from_str_radix(&num[..n], 10),
            Some((n, 'h')) | Some((n, 'H')) => u64::from_str_radix(&num[..n], 16),
            Some((n, 'k')) | Some((n, 'K')) => parse_oct_or_dec(&num[..n]).map(|v| v * 1024),
            Some((n, 'm')) | Some((n, 'M')) => parse_oct_or_dec(&num[..n]).map(|v| v * 1024 * 1024),
            Some((_, _)) => parse_oct_or_dec(num),
            None => panic!("num is empty"),
        })
        .parse_next(input)
}

pub fn number(input: &mut &[u8]) -> PResult<u64> {
    alt((prefixed_hex, suffixed_num)).parse_next(input)
}

#[cfg(test)]
pub fn arb_number() -> impl Strategy<Value = Vec<u8>> {
    proptest::string::bytes_regex(concat!(
        r"0x[0-9a-fA-F]{1,5}[kKmM]?",
        r"|[0-1]{1,5}[bB]",
        r"|[0-7]{1,5}[oO]",
        r"|[0-9]{1,5}[dD]",
        r"|[0-9a-fA-F]{1,5}[hH]",
        r"|([1-9][0-9]{1,5}|0[1-7]{1,5})[kKmM]?"
    ))
    .unwrap()
}

#[cfg(test)]
mod test {
    use super::*;

    proptest! {
        #[test]
        fn proptest_number(n in arb_number()) {
            assert!(number.parse(&n).is_ok());
        }
    }
    #[test]
    fn test_binary() {
        assert_eq!(number.parse(b"0b"), Ok(0));
        assert_eq!(number.parse(b"1101b"), Ok(0b1101));
        assert_eq!(number.parse(b"1101B"), Ok(0b1101));

        assert_eq!(
            number.parse(b"1111111111111111111111111111111111111111111111111111111111111111b"),
            Ok(0xffffffffffffffff)
        );
        assert_fail!(
            number.parse(b"10000000000000000000000000000000000000000000000000000000000000000b")
        );
        assert_eq!(
            number.parse(b"11111111111111111111111111111111b"),
            Ok(0xffffffff)
        );
        assert_eq!(
            number.parse(b"100000000000000000000000000000000b"),
            Ok(0x100000000)
        );

        assert_fail!(number.parse(b"2b"));
        assert_fail!(number.parse(b"ab"));

        assert_fail!(number.parse(b"1101bk"));
        assert_fail!(number.parse(b"1101bm"));
        assert_fail!(number.parse(b"1101Bk"));
        assert_fail!(number.parse(b"1101Bm"));
    }

    #[test]
    fn test_octal() {
        assert_eq!(number.parse(b"0o"), Ok(0));
        assert_eq!(number.parse(b"123o"), Ok(0o123));
        assert_eq!(number.parse(b"123O"), Ok(0o123));

        assert_eq!(number.parse(b"0123k"), Ok(0o123 * 1024));
        assert_eq!(number.parse(b"0123K"), Ok(0o123 * 1024));
        assert_eq!(number.parse(b"0123m"), Ok(0o123 * 1024 * 1024));
        assert_eq!(number.parse(b"0123M"), Ok(0o123 * 1024 * 1024));

        assert_eq!(
            number.parse(b"1777777777777777777777o"),
            Ok(0xffffffffffffffff)
        );
        assert_fail!(number.parse(b"2000000000000000000000o"));
        assert_eq!(number.parse(b"37777777777o"), Ok(0xffffffff));
        assert_eq!(number.parse(b"40000000000o"), Ok(0x100000000));

        assert_fail!(number.parse(b"8o"));
        assert_fail!(number.parse(b"ao"));

        assert_fail!(number.parse(b"123ok"));
        assert_fail!(number.parse(b"123om"));
        assert_fail!(number.parse(b"123Ok"));
        assert_fail!(number.parse(b"123Om"));
    }

    #[test]
    fn test_decimal() {
        assert_eq!(number.parse(b"0"), Ok(0));
        assert_eq!(number.parse(b"0d"), Ok(0));
        assert_eq!(number.parse(b"123"), Ok(123));
        assert_eq!(number.parse(b"123d"), Ok(123));
        assert_eq!(number.parse(b"123D"), Ok(123));

        assert_eq!(number.parse(b"123k"), Ok(123 * 1024));
        assert_eq!(number.parse(b"123K"), Ok(123 * 1024));
        assert_eq!(number.parse(b"123m"), Ok(123 * 1024 * 1024));
        assert_eq!(number.parse(b"123M"), Ok(123 * 1024 * 1024));

        assert_eq!(
            number.parse(b"18446744073709551615"),
            Ok(0xffffffffffffffff)
        );
        assert_fail!(number.parse(b"18446744073709551616"));
        assert_eq!(number.parse(b"4294967295"), Ok(0xffffffff));
        assert_eq!(number.parse(b"4294967296"), Ok(0x100000000));

        assert_eq!(number.parse(b"18014398509481983k"), Ok(0xfffffffffffffc00));
        assert_eq!(number.parse(b"17592186044415m"), Ok(0xfffffffffff00000));

        assert_fail!(number.parse(b"ad"));
        assert_fail!(number.parse(b"fd"));

        assert_fail!(number.parse(b"123dk"));
        assert_fail!(number.parse(b"123dm"));
    }

    #[test]
    fn test_hexadecimal() {
        assert_eq!(number.parse(b"0h"), Ok(0));
        assert_eq!(number.parse(b"0x0"), Ok(0));
        assert_eq!(number.parse(b"0xafd"), Ok(0xafd));
        assert_eq!(number.parse(b"0X0"), Ok(0x0));
        assert_eq!(number.parse(b"0XFD"), Ok(0xFD));
        assert_eq!(number.parse(b"123h"), Ok(0x123));
        assert_eq!(number.parse(b"123H"), Ok(0x123));

        assert_eq!(number.parse(b"a123h"), Ok(0xa123));
        assert_eq!(number.parse(b"A123H"), Ok(0xA123));

        assert_eq!(number.parse(b"0xafdk"), Ok(0xafd * 1024));
        assert_eq!(number.parse(b"0xafdK"), Ok(0xafd * 1024));
        assert_eq!(number.parse(b"0xafdm"), Ok(0xafd * 1024 * 1024));
        assert_eq!(number.parse(b"0xafdM"), Ok(0xafd * 1024 * 1024));

        assert_eq!(number.parse(b"0xffffffffffffffff"), Ok(0xffffffffffffffff));
        assert_fail!(number.parse(b"0x10000000000000000"));
        assert_eq!(number.parse(b"0xffffffff"), Ok(0xffffffff));
        assert_eq!(number.parse(b"0x100000000"), Ok(0x100000000));

        assert_eq!(number.parse(b"0x3fffffffffffffk"), Ok(0xfffffffffffffc00));
        assert_eq!(number.parse(b"0xfffffffffffm"), Ok(0xfffffffffff00000));

        assert_fail!(number.parse(b"123hk"));
        assert_fail!(number.parse(b"123hm"));
        assert_fail!(number.parse(b"123HK"));
        assert_fail!(number.parse(b"123HM"));
        assert_fail!(number.parse(b"0x123h"));
    }
}
