use clap::Parser;
use ldscript::{parse, LdCtx, LdWrite};
use std::path::PathBuf;

#[derive(Parser)]
struct Args {
    #[arg(long, short)]
    rewrite: bool,
    file: PathBuf,
}

fn main() {
    let args = Args::parse();

    let bytes = std::fs::read(args.file).unwrap();
    let parsed = parse(&bytes).unwrap();
    if args.rewrite {
        let mut ctx = LdCtx::new(std::io::stdout());
        for r in parsed {
            r.ld_write(&mut ctx).unwrap();
        }
    } else {
        println!("{:#?}", parsed);
    }
}
